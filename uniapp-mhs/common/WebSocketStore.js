import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


export default new Vuex.Store({
    state: {
        socketTask: null, // ws链接
        webSocketPingTimer: null, // 心跳定时器
        webSocketPingTime: 10000, // 心跳的间隔，当前为 10秒,
        webSocketReconnectCount: 0, // 重连次数
        webSocketIsReconnect: true, // 是否重连
        webSocketIsOpen: true,
        uid: null, //ws登录userId
        token: null, //ws登录token
        msg: [], //接收到的信息列表
        newAccept: null, //接受到最新的消息
        users: [], //聊天室用户集合
    },
    getters: {
        // 获取接收的信息
        socketMsgs: state => {
            return state.msg
        },
        //获取最新的消息map
        getAccept: state => {
            return state.newAccept
        }
    },
    mutations: {
        //发送http请求登录后设置用户id 用于ws登录
        setUid(state, uid) {
            state.uid = uid
        },
        addUsers(state, user) {
            state.users.push(user);
        },
        webSocketInit(state, uid) {
            let that = this
            //state.newAccept = new Map();
            // 创建一个this.socketTask对象【发送、接收、关闭socket都由这个对象操作】
            state.socketTask = uni.connectSocket({
                url: "ws://127.0.0.1:800/mhs/chat/" + uid,
                header: {
                    token: uni.getStorageSync('token')
                },
                success(data) {
                    state.uid = uid;
                    console.log("websocket连接成功 用户" + state.uid);
                },
                fail(data) {
                    console.log("websocket连接失败");
                }
            });
            state.socketTask.onMessage(function (res) {
                var a = JSON.parse(res.data)
                state.newAccept = a;
                //state.msg.push()
                console.log("开始输出");
                console.info(state.newAccept)


            });


            /* 	// ws连接开启后登录验证
                    state.socketTask.onOpen((res) => {
                        console.log("WebSocket连接正常打开中...！");
                        that.commit('webSocketLogin')
                        //开始心跳
                        that.commit('webSocketPing')
                        // 注：只有连接正常打开中 ，才能正常收到消息
                        state.socketTask.onMessage((res) => {
                            console.log("收到服务器内容：" + res.data);
                            state.msg = JSON.parse(res.data)
                        });

                    });

                    // 链接开启后登录验证
                    state.socketTask.onError((errMsg) => {
                        console.log(errMsg)
                        console.log("ws连接异常")
                        that.commit('webSocketClose')
                    }); */

                    // 链接开启后登录验证
                    state.socketTask.onClose((errMsg) => {
                        console.log(errMsg)
                        console.log("ws连接关闭")
                        that.commit('webSocketClose')
                    });

        },
        checkConnection(state, uid) {
            if (state.socketTask == null) {
                state.socketTask = uni.connectSocket({
                    url: "ws://127.0.0.1:800/mhs/chat/" + uid,
                    success(data) {
                        state.uid = uid;
                        console.log("websocket重新连接 用户" + state.uid);
                    },
                    fail(data) {
                        console.log("websocket连接失败");
                    }
                });
                state.socketTask.onMessage(function (res) {
                    var a = JSON.parse(res.data)
                    state.newAccept = a;
                    //state.msg.push()
                    console.log("开始输出");
                    console.info(state.newAccept)
                });
            }

        },


        // 发送ws消息
        webSocketSend(state, payload) {
            console.log(payload + "     这里")
            let that = this
            that.state.socketTask.send({
                data: payload,
                header: {
                    token: uni.getStorageSync('token')
                },
                fail: function (res) {
                    console.log("发送失败")
                    that.state.sendMsgStatus = true
                },
                success: function (res) {
                    console.log("发送成功")
                    that.state.sendMsgStatus = false
                }
            })

        }
    },


    actions: {
        /* webSocketInit({
            commit
        }, url) {
            commit('webSocketInit', url)
        },
        webSocketSend({
            commit
        }, p) {
            commit('webSocketSend', p)
        } */
    }
})