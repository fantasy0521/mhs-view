import axios from 'axios'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import {Message} from 'element-ui'

//项目后台
const request = axios.create({
	// baseURL: 'http://127.0.0.1:8090/admin/',//Linux ip  http://192.168.138.100:8090/
	baseURL: 'http://127.0.0.1:800/mhs',//Linux ip  http://192.168.138.100:8090/
	timeout: 5000
})

let CancelToken = axios.CancelToken

// 请求拦截
request.interceptors.request.use(config => {
		//对于访客模式，除GET请求外，都拦截并提示
		const userJson = window.localStorage.getItem('user') || '{}'
		const role = window.localStorage.getItem('role') || '{}'
    console.log(role);
		const user = JSON.parse(userJson)
		// if (userJson !== '{}' && user.role !== 'ROLE_admin' && config.method !== 'get'){
		if (userJson !== '{}' && role !== 'admin' && config.method !== 'get') {
			config.cancelToken = new CancelToken(function executor(cancel) {
				cancel('演示模式，不允许操作')
			})
			return config
		}

		NProgress.start()
		const token = window.localStorage.getItem('token')
		if (token) {
			// config.headers.Authorization = token
			config.headers.token = token
		}
		return config
	},
	error => {
		console.info(error)
		return Promise.reject(error)
	}
)

// 响应拦截
request.interceptors.response.use(response => {
		NProgress.done()
		const res = response.data
		// if (res.code != 200) {
		if(res.code == 401){
			Message.error(res.msg || 'Error');
			//token失效,跳转到登陆
			console.log("token失效,正在跳转登陆页")
			//清空token和user
			window.localStorage.clear()
			// this.$router.push('login')
			window.location.href="../../login"
		}
		if (res.code != 1 && res.code != 200) {
			Message.error(res.msg || 'Error')
		}
		// return res
		return response
	},
	error => {
		console.info(error)
		Message.error(error.message)
		return Promise.reject(error)
	}
)

export default request