import axios from '@/util/request'

//查询user列表
export function getUserList(queryInfo) {
	return axios({
		url: 'user/getUserList',
		method: 'GET',
		data: {
			...queryInfo
		}
	})
}

//修改用户信息
export function updateUser(userVo) {
	return axios({
		url: 'user/updateUser',
		method: 'POST',
		data: {
			...userVo
		}
	})
}

//查询咨询师申请列表
export function getApplyList(queryInfo) {
	return axios({
		url: 'consultation/queryByPage',
		method: 'GET',
		data: {
			...queryInfo
		}
	})
}

//修改咨询师信息
export function updateStatus(userVo) {
	return axios({
		url: 'consultation/updateStatus',
		method: 'POST',
		data: {
			...userVo
		}
	})
}

//删除咨询申请
export function deleteByCid(cid) {
	return axios({
		url: 'consultation/deleteByCid?cid='+cid,
		method: 'GET'
	})
}