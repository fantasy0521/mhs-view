import axios from '@/util/request'

export function getDashboard() {
	return axios({
		url: 'http://106.14.45.117:8090/admin/dashboard',
		method: 'GET'
	})
}