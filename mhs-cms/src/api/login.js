import axios from '@/util/request'

export function login(loginForm) {
	return axios({
		url: 'login',
		method: 'POST',
		data: {
			...loginForm
		}
	})
}

// 管理员登陆
export function loginAdmin(loginForm) {
	return axios({
		url: 'user/loginAdmin',
		method: 'POST',
		data: {
			...loginForm
		}
	})
}